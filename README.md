Compiler Final Project   
In this final project, your goal is to craft a Mini-LISP interpreter.    
All resources you need are on the LMS, including the language specifications, a standard interpreter (called smli) and some Mini-LISP program examples for testing.    
Since you have learnt lex and yacc, it’s good to use these tools to build your project, but you are allowed to use other languages and tools for this project.    

Your tasks are to    
1. Read the language specifications of Mini-LISP.    
2. Read and run Mini-LISP program examples to understand the language behavior.    
3. Write an interpreter for Mini-LISP, implement features of the language.    
 How to run the standard interpreter? $ ./smli example.lsp    
Project Grade    
For each features, there are 2 public test cases and 2 hidden test cases. You can get 80% of the score by passing public test cases, and 20% for hidden test cases.    
Basic Features      
    
Feature    
Description   
 
1. Syntax Validation   
Print “syntax error” when parsing invalid syntax    
 
2. Print   
Implement print-num statement   

3. Numerical Operations   
Implement all numerical operations   

4. Logical Operations   
Implement all logical operations   

5. if Expression   
Implement if expression   

6. Variable Definition   
Able to define a variable   

7. Function   
Able to declare and call an anonymous function   

8. Named Function   
Able to declare and call a named function  